module.exports = {
  testEnvironment: 'node',
  coveragePathIgnorePatterns: ["/node_modules/"],
  notify: true
};
