const express = require('express')
const pingRoute = require('./routes/ping')
const authRoutes = require('./routes/auth')
const bodyParser = require('body-parser')
const process = require('process')

const app = express()

app.use(bodyParser.json())

app.use('/', pingRoute)
app.use('/', authRoutes)

process.on('SIGINT', function onSigint() {
  app.shutdown();
})

process.on('SIGTERM', function onSigterm() {
  app.shutdown();
})

app.shutdown = function () {
  postgres.pool.end()
    .then(() => console.log('client has disconnected'))
    .catch(err => console.error('error during disconnection', err.stack));
  process.exit();
}

module.exports = app