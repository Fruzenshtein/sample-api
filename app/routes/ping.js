const express = require('express')
const asyncHandler = require('express-async-handler')

const router = express.Router()

router.get('/ping', asyncHandler(async(req, res) => {
  res.json({message: 'pong'})
}))

module.exports = router;
