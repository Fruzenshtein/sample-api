require('jest')
const server = require('../startServer')
const supertest = require('supertest')

const request = supertest(server)

test('GET /me endpoint with valid JWT-token', async done => {
  
  //JWT token for `{"name": "Joe","iss": "user_01"}`, using `super_secret_key`
  const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9lIiwiaXNzIjoidXNlcl8wMSJ9.Abos1oNb-KReuwXsts1Tmd2GtmSSlDPfC2u2lxJmJ10'

  const meResponse = await request
    .get('/me')
    .set('Authorization', `Bearer ${token}`)
  expect(meResponse.status).toBe(200)
  expect(meResponse.body.name).toBe('Joe')
  expect(meResponse.body.iss).toBe('user_01')

  done()
})

test('GET /me endpoint with invalid JWT-token', async done => {
  
  //JWT token for `{"name": "Joe","iss": "user_01"}`, using `super_secret_key`
  const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9lIiwiaXNzIjoidXNlcl8wMSJ9.QnB2ezJwyoCXl_O96YaLoRNMffapmHeTBA1QZD5eFYI'

  const meResponse = await request
    .get('/me')
    .set('Authorization', `Bearer ${token}`)
  expect(meResponse.status).toBe(403)

  done()
})

afterAll((done) => {
  server.close(done)
})
