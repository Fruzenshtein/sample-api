const express = require('express');
const asyncHandler = require('express-async-handler')

const authenticateJWT = require('../helpers/authMiddleware')

require('dotenv').config()

const router = express.Router()

router.get('/me', authenticateJWT, asyncHandler(async(req, res) => {
  res.json(req.user)
}))

module.exports = router;
