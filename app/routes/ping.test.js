require('jest')
const server = require('../startServer')
const supertest = require('supertest')

const request = supertest(server)

test('ping endpoint', async done => {
  const response = await request.get('/ping')
  expect(response.status).toBe(200)
  expect(response.body.message).toBe('pong')
  done()
})

afterAll((done) => {
  server.close(done)
})
